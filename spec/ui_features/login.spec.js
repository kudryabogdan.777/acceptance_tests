const LoginPage = require('../pages/login.page');
const DashboardPage = require('../pages/dashboard.page');
const TestData = require('../test_data');

describe('Login', () => {
    it('it is possible to log in with valid credentials', () => {
        LoginPage.open();
        LoginPage.login(TestData.USER, TestData.PASSWORD);

        expect(DashboardPage.productsAreDisplayed()).toBe(true);
    });

    it('locked user isn\'t able to login', () =>{
        LoginPage.open();
        LoginPage.login(TestData.LOCKED_USER, TestData.PASSWORD);

        expect(DashboardPage.productsAreDisplayed()).toBe(false);
        expect(LoginPage.errorMessage).toHaveTextContaining('Sorry, this user has been locked out');
    });

    it('it is not possible to login with not existing username or password', () =>{
        const invalidUsername = 'invalid';
        const invalidPass = 'invalidpass';

        LoginPage.open();
        LoginPage.login(invalidUsername, TestData.PASSWORD);

        expect(DashboardPage.productsAreDisplayed()).toBe(false);
        expect(LoginPage.errorMessage).toHaveTextContaining('Epic sadface: Username and password do not match any user in this service');

        LoginPage.login(TestData.USER, invalidPass);

        expect(DashboardPage.productsAreDisplayed()).toBe(false);
        expect(LoginPage.errorMessage).toHaveTextContaining('Epic sadface: Username and password do not match any user in this service');
    });
});


