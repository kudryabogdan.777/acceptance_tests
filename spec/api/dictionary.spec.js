const frisby = require('frisby');
const TestData = require('../../test_data');

frisby.globalSetup({
    request: {
        headers: {
            "Authorization": TestData.API_KEY,
            'Content-Type': 'application/json'
        }
    }
});

describe('Owlbot dictionary', () => {
    it('it is possible to get definition of the word: API', () => {
        return frisby.get('https://owlbot.info/api/v4/dictionary/api')
            .expect('status', 200)
            .then(function (res) {
                expect(res.json.word).toBe('api');
                expect(res.json.definitions[0].type).toBe('abbreviation');
                expect(res.json.definitions[0].definition).toBe('Abbreviation of application programming interface. APIs are usually being used between two or more application to communicate data or send commands to each other.')
                expect(res.json.definitions[0].example).toBe('Many mobile apps are using the Owlbot API to get definitions of English vocabularies.');
            });
    });

    it('it is possible to get definition of the word: Testing', () => {
        return frisby.get('https://owlbot.info/api/v4/dictionary/testing')
            .expect('status', 200)
            .then(function (res) {
                expect(res.json.word).toBe('testing');
                expect(res.json.pronunciation).toBe('test');
                expect(res.json.definitions[0].example).toBe('it\'s been quite a testing time for all of us');
                expect(res.json.definitions[0].definition).toBe('revealing a person\'s capabilities by putting them under strain; challenging.')
                expect(res.json.definitions[0].type).toBe('adjective');
            });
    });

    it('it is not possible to get definition of incorrect word: qwerty, status 404 is returned', () => {
        return frisby.get('https://owlbot.info/api/v4/dictionary/qwerty')
            .expect('status', 404)
            .then(function (res) {
                expect(res.json[0].message).toBe('No definition :(');
            });
    });
});


