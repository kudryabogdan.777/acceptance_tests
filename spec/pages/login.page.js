const Page = require('./common_page');

class LoginPage extends Page {
    get inputUsername () { return $('#user-name') }
    get inputPassword () { return $('#password') }
    get btnLogin () { return $('#login-button') }
    get errorMessage () { return $('h3[data-test="error"]') }

    login (username, password) {
        this.inputUsername.setValue(username);
        this.inputPassword.setValue(password);
        this.btnLogin.click();
    }

    open () {
        return super.open();
    }
}

module.exports = new LoginPage();
