const Page = require('./common_page');

class DashboardPage extends Page {
    productsAreDisplayed () {
        const el = $('.product_label');
        return el.isDisplayed();
    }
}

module.exports = new DashboardPage();
