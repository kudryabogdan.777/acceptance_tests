# Acceptance tests

This project is an example of how to get started using WebdriverIO for Selenium testing using node.js. It makes use of Jasmine as a BDD framework.
Page object pattern is used in this example. Page Object is a class that represents a web page and hold the functionality and members.
This Page class will identify the WebElements of that web page and also contains Page methods which perform operations on those WebElements.
(Website: www.saucedemo.com).

For API testing is used Frisby.js that makes testing API endpoints easy and fast. (API service: https://owlbot.info/).
                        
## Requirements

* Node.js https://nodejs.org/en/download/ 
* Google Chrome

Install dependencies by: (from *acceptance_tests* folder)

```
npm install
```

## Run the UI specs

From *acceptance_tests* run:

```
npx wdio run wdio.conf.js
```

If you like to run specific test files you can add a --spec parameter:

```
npx wdio run ./wdio.conf.js --spec login.spec.js
```

## Run the API specs

From *acceptance_tests* run:

```
jasmine test/spec/api/dictionary.spec.js
```

## HTML reports
You can view Html reports after the test run. Html reports are located in 'reports' folder

## Useful links

* WDIO: https://webdriver.io/ 
* Jasmine: https://jasmine.github.io
* Frisby: https://docs.frisbyjs.com/ 
* List of ChromeOptions: https://peter.sh/experiments/chromium-command-line-switches/
* List of ChromePrefs: https://chromium.googlesource.com/chromium/src/+/8eaec82caaae926faedbfd7df0beb4979c3f6792/components/content_settings/core/common/pref_names.cc
